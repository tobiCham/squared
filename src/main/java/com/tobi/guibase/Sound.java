package com.tobi.guibase;

public interface Sound
{
	public void load(String audioName);
	
	public void play();
	public void stop();
	public void pause();
	
	public void setVolume(float vol);
	public float getVolume();
	
	public void setPosition(int framePosition);
	public int getPosition();
	
	public boolean isPlaying();
	
	public void setPlayable(boolean enabled);
	public boolean isPlayable();
}

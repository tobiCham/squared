package com.tobi.guibase;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

//Class to call events when the window is closed or opened
public class WindowEvents implements WindowListener {

	private GUIBase base;
	
	public WindowEvents(GUIBase base) {
		this.base = base;
	}
	
	@Override
	public void windowOpened(WindowEvent e) {
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		base.windowClosed();
	}

	@Override
	public void windowClosed(WindowEvent e) {
		base.windowClosed();
	}

	@Override
	public void windowIconified(WindowEvent e) {
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		
	}
}

package com.tobi.guibase;

//Class which will call a method FPS times a second
public class UpdateTicker {
	
	private UpdateTickEvent updateTickEvent;
	private int targetDelay;
	
	private boolean stop = false;
	
	public UpdateTicker(int fps, UpdateTickEvent updateTickEvent) {
		this.updateTickEvent = updateTickEvent;
		setFPS(fps);
	}
	
	public void start() {
		startUpdate();
	}
	
	public void stop() {
		stop = true;
	}
	
	public void unlimitFPS() {
		targetDelay = 0;
	}
	
	public void setFPS(int newFps) {
		if(newFps <= 0) targetDelay = 0;
		else targetDelay = 1000000000 / newFps;
	}
	
	private void startUpdate() {
		long lastTickTime = System.nanoTime() - targetDelay;
		
		while(!stop) {
			long time = System.nanoTime();
			int lastTickDuration = (int) (time - lastTickTime);
			lastTickTime = time;
			
			updateTickEvent.onTickEvent(lastTickDuration);
			
			long updateDuration = System.nanoTime() - time;
			if(updateDuration >= targetDelay) continue;
			
			long toWait = targetDelay - updateDuration;
			delay(toWait);
		}
		stop = false;
	}
	
	private void delay(long nano) {
		long time = System.nanoTime();
		float millis = (nano / 1000000f) - 1;
		if(millis > 1) {
			int roundedMillis = (int) millis;
			try {
				Thread.sleep(roundedMillis);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		long newTime = System.nanoTime();
		while(newTime - time < nano) newTime = System.nanoTime();
	}
}

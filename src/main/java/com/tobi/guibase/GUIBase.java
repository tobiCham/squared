package com.tobi.guibase;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints.Key;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.UIManager;

/**
 * init() needs to be called first, followed by open() to start running the
 * application
 */
public abstract class GUIBase {
	
	private JFrame frame;
	private JEditorPane pane;
	
	private Keyboard keyboard;
	private Mouse mouse;
	
	private int width, height;
	
	private boolean maintainAspectRatio = false;
	private int renderX, renderY, renderWidth, renderHeight;
	private boolean ignoresFocussed = false;
	
	private RenderSetting renderSetting = RenderSetting.DEFAULT;
	
	private UpdateTicker ticker;
	
	public GUIBase(int width, int height, boolean resizable, boolean maximised, String title) {
		this.width = width;
		this.height = height;
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			System.err.println("Failed to load look and feel: " + e.getMessage());
		}
		
		frame = new JFrame();
		//Every time the paint method is called, it renders the display
		pane = new JEditorPane() {
			@Override
			public void paintComponent(Graphics g) {
				interalRender(g);
			}
		};
		
		pane.setPreferredSize(new Dimension(width, height));
		frame.add(pane);
		frame.pack();
		
		frame.setTitle(title);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(resizable);
		
		frame.addWindowListener(new WindowEvents(this));
		
		keyboard = new Keyboard(this);
		pane.addKeyListener(keyboard);
		
		mouse = new Mouse(this);
		pane.addMouseListener(mouse);
		pane.addMouseMotionListener(mouse);
		pane.addMouseWheelListener(mouse);
		
		frame.setLocationRelativeTo(null);
		if (maximised) frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		ticker = new UpdateTicker(30, new UpdateTickEvent() {
			@Override
			public void onTickEvent(int time) {
				pane.repaint();
				update(time);
			}
		});
	}
	
	//Applies rendering hints which change how th game is rendered
	public static final void applySettings(Graphics2D g, RenderSetting setting) {
		for(Key key : setting.getRenderOptions().keySet()) {
			g.setRenderingHint(key, setting.getRenderOptions().get(key));
		}
	}
	
	public void open() {
		new Thread() {
			@Override
			public void run() {
				ticker.start();
			}
		}.start();
		frame.setVisible(true);
	}
	
	private void interalRender(Graphics g2) {
		Graphics2D g = (Graphics2D) g2;
		
		applySettings(g, renderSetting);
		
		int paneWidth = pane.getWidth();
		int paneHeight = pane.getHeight();
		
		//Clear the background
		g.clearRect(0, 0, paneWidth, paneHeight);
		
		renderBackground(g, paneWidth, paneHeight);
		
		//If the frame isn't resizable, render to the screen and return
		if (!frame.isResizable()) {
			render(g);
			return;
		}
		
		float scaleFactorX = 1;
		float scaleFactorY = 1;
		
		//If the program should maintain its aspect ratio, do some calculations to offset the rendering and set the scale
		if (maintainAspectRatio) {
			float scaleFactor = (float) paneWidth / getWidth();
			int newHeight = (int) (getHeight() * scaleFactor);
			if (newHeight > paneHeight) scaleFactor = (float) paneHeight / getHeight();
			int newWidth = (int) (scaleFactor * getWidth());
			newHeight = (int) (scaleFactor * getHeight());
			
			renderX = (paneWidth / 2) - (newWidth / 2);
			renderY = (paneHeight / 2) - (newHeight / 2);
			renderWidth = newWidth;
			renderHeight = newHeight;
			scaleFactorX = scaleFactor;
			scaleFactorY = scaleFactor;
			
			g.clipRect(renderX, renderY, renderWidth, renderHeight);
		} else {
			//Otherwise, set the scale so that the rendering is stretched to the size of the screen
			renderX = 0;
			renderY = 0;
			renderWidth = pane.getWidth();
			renderHeight = pane.getHeight();
			scaleFactorX = (float) renderWidth / getWidth();
			scaleFactorY = (float) renderHeight / getHeight();
		}
		
		//Translate and scale where everything is being rendered
		g.translate(renderX, renderY);
		g.scale(scaleFactorX, scaleFactorY);
		
		//Renders the whole game
		render(g);
		//Undo the scale and translation
		g.scale(1f/scaleFactorX, 1f/scaleFactorY);
		g.translate(-renderX, -renderY);
	}
	
	/**
	 * Renders the screen
	 */
	public abstract void render(Graphics g);
	
	public void renderBackground(Graphics g, int paneWidth, int paneHeight) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, paneWidth, paneHeight);
	}
	
	/**
	 * Will try to update FPS times per second
	 */
	public abstract void update(int updateTime);
	
	public JFrame getFrame() {
		return frame;
	}
	
	public UpdateTicker getTicker() {
		return ticker;
	}
	
	public JEditorPane getRenderPane() {
		return pane;
	}
	
	public Mouse getMouse() {
		return mouse;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public boolean doesMaintainAspectRatio() {
		return maintainAspectRatio;
	}
	
	public void setMaintainAspectRatio(boolean maintainAspectRatio) {
		this.maintainAspectRatio = maintainAspectRatio;
	}
	
	public int getRenderX() {
		return renderX;
	}
	
	public int getRenderY() {
		return renderY;
	}
	
	public int getRenderWidth() {
		return renderWidth;
	}
	
	public int getRenderHeight() {
		return renderHeight;
	}
	
	public boolean doesIgnorsFocussed() {
		return ignoresFocussed;
	}

	public void setIgnoresFocussed(boolean ignoresFocussed) {
		this.ignoresFocussed = ignoresFocussed;
	}

	public RenderSetting getRenderSetting() {
		return renderSetting;
	}

	public void setRenderSetting(RenderSetting renderSetting) {
		this.renderSetting = renderSetting;
	}

	public void resize(int newWidth, int newHeight) {
		this.width = newWidth;
		this.height = newHeight;
	}

	public void keyPress(int keycode, char ch) {}
	public void keyRelease(int keycode, char ch) {}
	public void mouseClick(int mouseButton) {}
	public void mouseRelease(int mouseButton) {}
	public void mouseDrag() {}
	public void mouseScroll(int scrollPosition) {}
	public void windowClosed() {}
}

package com.tobi.guibase;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.HashSet;
import java.util.Set;

public class Mouse extends MouseAdapter implements MouseWheelListener {
	
	private int mouseX;
	private int mouseY;
	public static final int MOUSE_LEFT = 1;
	public static final int MOUSE_MIDDLE = 2;
	public static final int MOUSE_RIGHT = 3;
	private Set<Integer> buttonsDown = new HashSet<Integer>();
	private GUIBase base;
	
	public Mouse(GUIBase base) {
		this.base = base;
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		mouseX = e.getX();
		mouseY = e.getY();
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		buttonsDown.add(e.getButton());
		mouseX = e.getX();
		mouseY = e.getY();
		base.mouseDrag();
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		mouseX = e.getX();
		mouseY = e.getY();
		buttonsDown.add(e.getButton());
		base.mouseClick(e.getButton());
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		mouseX = e.getX();
		mouseY = e.getY();
		buttonsDown.remove(e.getButton());
		base.mouseRelease(e.getButton());
	}
	
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		mouseX = e.getX();
		mouseY = e.getY();
		base.mouseScroll(e.getWheelRotation());
	}
	
	/**
	 * This returns the exact X coordinate of the mouse; doesn't matter about
	 * window size.
	 */
	public int getExactX() {
		return mouseX;
	}
	
	/**
	 * This returns the exact Y coordinate of the mouse; doesn't matter about
	 * window size.
	 */
	public int getExactY() {
		return mouseY;
	}
	
	/**
	 * This returns the relative X coordinate of the mouse.
	 */
	public int getX() {
		if(!base.getFrame().isResizable()) return mouseX;
		if (base.doesMaintainAspectRatio()) {
			double presumeX = (double) mouseX - (base.getRenderX());
			double ratio = (double) base.getRenderWidth() / base.getWidth();
			return (int) (presumeX / ratio);
		} else {
			double ratio = base.getRenderPane().getWidth() / (double) base.getWidth();
			double relative = (double) mouseX / ratio;
			return (int) relative;
		}
	}
	
	/**
	 * This returns the relative Y coordinate of the mouse.
	 */
	public int getY() {
		if (base.doesMaintainAspectRatio()) {
			double presumeY = (double) mouseY - (base.getRenderY());
			double ratio = (double) base.getRenderHeight() / base.getHeight();
			return (int) (presumeY / ratio);
		} else {
			double ratio = base.getRenderPane().getHeight() / (double) base.getHeight();
			double relative = (double) mouseY / ratio;
			return (int) relative;
		}
	}
	
	public void setX(int x) {
		this.mouseX = x;
	}
	
	public void setY(int y) {
		this.mouseY = y;
	}
	
	public void releaseButton(int button) {
		buttonsDown.remove(button);
	}
	
	public void releaseAllButtons() {
		buttonsDown.clear();
	}
	
	public boolean isButtonDown(int button) {
		return buttonsDown.contains(button);
	}
}

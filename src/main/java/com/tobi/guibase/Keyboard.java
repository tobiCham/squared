package com.tobi.guibase;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.Set;

public class Keyboard implements KeyListener {
	
	private GUIBase app;
	public static Set<Integer> keysDown = new HashSet<Integer>();
	
	public Keyboard(GUIBase application) {
		this.app = application;
	}

	@Override
	public void keyPressed(KeyEvent event) {
		keysDown.add(event.getKeyCode());
		app.keyPress(event.getKeyCode(), event.getKeyChar());
	}
	
	@Override
	public void keyReleased(KeyEvent event) {
		keysDown.remove(event.getKeyCode());
		app.keyRelease(event.getKeyCode(), event.getKeyChar());
	}
	
	@Override
	public void keyTyped(KeyEvent event) {}
	
	public static boolean isKeyDown(int key) {
		return keysDown.contains(key);
	}
	
	public static void releaseKey(int key) {
		keysDown.remove(key);
	}
	
	public static Set<Integer> getKeysDown() {
		return keysDown;
	}
	
	public static void releaseAllKeys() {
		keysDown.clear();
	}
}

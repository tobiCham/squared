package com.tobi.guibase;

public interface UpdateTickEvent {
	
	//Method called when the update ticker ticks
	public void onTickEvent(int time);
}

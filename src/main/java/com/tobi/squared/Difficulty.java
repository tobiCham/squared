package com.tobi.squared;

public enum Difficulty {
	
	EASY(16), MEDIUM(16), HARD(16);
	
	private int levels;
	
	private Difficulty(int levels) {
		this.levels = levels;
	}
	
	public int getLevels() {
		return levels;
	}
	
	public int getStartLevel() {
		int counter = 0;
		for (Difficulty difficulty : values()) {
			if (difficulty == this) { return counter; }
			counter += difficulty.getLevels();
		}
		return 0;
	}
	
	public int getEndLevel() {
		int counter = 0;
		for (Difficulty difficulty : values()) {
			counter += difficulty.getLevels();
			if (difficulty == this) { return counter; }
		}
		return 0;
	}
	
	public static Difficulty getByLevelNumber(int level) {
		for (Difficulty difficulty : values()) {
			int begin = difficulty.getStartLevel();
			int end = difficulty.getEndLevel();
			if (level >= begin && level < end) return difficulty;
		}
		return null;
	}
}

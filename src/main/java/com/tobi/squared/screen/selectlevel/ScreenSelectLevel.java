package com.tobi.squared.screen.selectlevel;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import com.tobi.guibase.Mouse;
import com.tobi.squared.Difficulty;
import com.tobi.squared.GameResources;
import com.tobi.squared.OptionButton;
import com.tobi.squared.Squared;
import com.tobi.squared.data.LevelData;
import com.tobi.squared.screen.Screen;
import com.tobi.squared.sound.Sounds;

public class ScreenSelectLevel extends Screen {
	
	private Difficulty difficulty;
	private List<ButtonSelectLevel> buttons = new ArrayList<ButtonSelectLevel>();
	private OptionButton<?> backButton;
	
	public ScreenSelectLevel(Squared game, Difficulty difficulty) {
		super(game);
		this.difficulty = difficulty;
		int backButtonWidth = 900;
		
		backButton = new OptionButton<Object>(getGame(), (getGame().getWidth() / 2) - (backButtonWidth / 2), 860, backButtonWidth, 100, Color.red, Color.red, Color.white, "Back to Menu", new Object[0]);
		backButton.setOnClick(new Runnable() {
			@Override
			public void run() {
				Screen.setSelectedScreen(GameResources.selectDifficulty);
			}
		});
		backButton.setRoundnessRatio(0.1f);
	}
	
	@Override
	public void render(Graphics g) {
		g.setColor(Color.black);
		Font oldFont = g.getFont();
		g.setFont(new Font(oldFont.getFontName(), Font.PLAIN, 30));
		g.fillRect(0, 0, getGame().getWidth(), getGame().getHeight());
		boolean any = false;
		for (ButtonSelectLevel button : buttons) {
			button.render(g);
			LevelData data = button.getLevelData();
			if (data == null || !data.isUnlocked()) g.drawImage(GameResources.padlock, button.getX() + button.getWidth() + 10, button.getY() + (button.getHeight() - 64) / 2, 50, 64, null);
			if (button.isHovered()) any = true;
		}
		backButton.render(g);
		if (backButton.isHovered()) any = true;
		getDisplay().getRenderPane().setCursor(Cursor.getPredefinedCursor(any ? Cursor.HAND_CURSOR : Cursor.DEFAULT_CURSOR));
	}
	
	@Override
	public void update(double delta) {}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getMouse();
		backButton.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		for (ButtonSelectLevel button : buttons) {
			button.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		}
	}
	
	@Override
	public void keyPressed(int key, char ch) {
		if (key == KeyEvent.VK_ESCAPE) Screen.setSelectedScreen(GameResources.selectDifficulty);
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		Sounds.levelMusic.setPosition(0);
		int gapX = 110;
		int gapY = 50;
		int width = 140;
		int height = 150;
		
		int totalWidth = (4 * (width + gapX)) - gapX;
		int indentX = (getGame().getWidth() - totalWidth) / 2;
		int indentY = 50;
		
		for (int i = difficulty.getStartLevel() + 1; i <= difficulty.getEndLevel(); i++) {
			int x = (i - difficulty.getStartLevel() - 1) % 4;
			int y = (i - difficulty.getStartLevel() - 1) / 4;
			
			LevelData data = Squared.getInstance().getDatabase().getData(i);
			if (data == null) data = new LevelData(i, 0, false);
			ButtonSelectLevel level = new ButtonSelectLevel(getGame(), indentX + (x * (width + gapX)), indentY + (y * (height + gapY)), width, height, data);
			buttons.add(level);
		}
	}
}

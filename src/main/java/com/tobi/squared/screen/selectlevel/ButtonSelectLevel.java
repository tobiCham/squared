package com.tobi.squared.screen.selectlevel;

import java.awt.Color;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.tobi.squared.OptionButton;
import com.tobi.squared.Squared;
import com.tobi.squared.data.LevelData;
import com.tobi.squared.screen.Screen;
import com.tobi.squared.screen.ScreenLevel;

public class ButtonSelectLevel extends OptionButton<String> {
	
	private LevelData data;
	
	public ButtonSelectLevel(Squared game, int x, int y, int width, int height, LevelData data) {
		super(game, x, y, width, height, Color.red, Color.red, Color.white, "Level " + data.getLevel(), new String[0]);
		this.data = data;
		
		setRoundnessRatio(0.7f);
		setOnClick(new Runnable() {
			
			@Override
			public void run() {
				if (getLevelData() == null || !getLevelData().isUnlocked()) return;
				enterLevel();
			}
		});
		
		if (!data.isUnlocked() || data.getBestTime() <= 0f) return;
		
		BigDecimal time = new BigDecimal(data.getBestTime());
		if (time.floatValue() < 10) time = time.setScale(1, RoundingMode.HALF_UP);
		else time = time.setScale(0, RoundingMode.HALF_UP);
		
		String str = time.toString();
		int minutes = (int) (data.getBestTime() / 60);
		if (minutes > 0) {
			String strSeconds = ((int) (data.getBestTime()) % 60) + "";
			if (strSeconds.length() == 1) strSeconds = "0" + strSeconds;
			str = minutes + ":" + strSeconds;
		}
		
		setHoverText("Best Time: " + str);
	}
	
	public LevelData getLevelData() {
		return data;
	}
	
	public void enterLevel() {
		if (data == null) return;
		ScreenLevel screen = Squared.getInstance().getLoader().loadLevel(data.getLevel());
		if (screen != null) Screen.setSelectedScreen(screen);
	}
}

package com.tobi.squared.screen.title;

import java.awt.Rectangle;

public class TitleButton {
	
	private Rectangle bounds;
	private String name;
	private Runnable runnable;
	
	public TitleButton(Rectangle bounds, String name, Runnable runnable) {
		this.bounds = bounds;
		this.name = name;
		this.runnable = runnable;
	}
	
	public Rectangle getBounds() {
		return bounds;
	}
	
	public void setBounds(Rectangle bounds) {
		this.bounds = bounds;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Runnable getRunnable() {
		return runnable;
	}
	
	public void setRunnable(Runnable runnable) {
		this.runnable = runnable;
	}
}

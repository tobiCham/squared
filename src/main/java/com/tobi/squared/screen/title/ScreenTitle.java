package com.tobi.squared.screen.title;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import com.tobi.guibase.Mouse;
import com.tobi.squared.GameResources;
import com.tobi.squared.ResourceLoader;
import com.tobi.squared.Squared;
import com.tobi.squared.Transition;
import com.tobi.squared.screen.Screen;
import com.tobi.squared.sound.Sounds;

public class ScreenTitle extends Screen {
	
	private Image image;
	private List<TitleButton> buttons = new ArrayList<TitleButton>();
	
	public ScreenTitle(Squared game) {
		super(game);
		
		image = ResourceLoader.loadImage("background.png");
		
		int width = 525;
		int height = 100;
		int gap = 150;
		
		buttons.add(new TitleButton(new Rectangle(0, 400, width, height), "Play Game", new Runnable() {
			
			@Override
			public void run() {
				Screen.setSelectedScreen(GameResources.selectDifficulty);
			}
		}));
		
		buttons.add(new TitleButton(new Rectangle(0, 400 + gap, width, height), "Level Designer", new Runnable() {
			
			@Override
			public void run() {
				Screen.setSelectedScreen(GameResources.levelDesigner);
			}
		}));
		
		buttons.add(new TitleButton(new Rectangle(0, 400 + (gap * 2), width, height), "Settings", new Runnable() {
			
			@Override
			public void run() {
				Screen.setSelectedScreen(GameResources.screenOptions);
			}
		}));
		
		buttons.add(new TitleButton(new Rectangle(0, 400 + (gap * 3), width, height), "Quit Game", new Runnable() {
			
			@Override
			public void run() {
				getGame().getDisplay().windowClosed();
				System.exit(0);
			}
		}));
	}
	
	@Override
	public void render(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getGame().getWidth(), getGame().getWidth());
		
		Font oldFont = g.getFont();
		
		g.setColor(Color.WHITE);
		
		g.setFont(new Font(oldFont.getFontName(), Font.PLAIN, 180));
		String title = getDisplay().getFrame().getTitle();
		int textWidth = g.getFontMetrics(g.getFont()).getStringBounds(title, g).getBounds().width;
		
		g.drawString(title, (getGame().getWidth() / 2) - (textWidth / 2), 275);
		
		g.setFont(new Font(oldFont.getFontName(), Font.PLAIN, 60));
		
		boolean any = false;
		Mouse mouse = getMouse();
		Rectangle mouseRect = new Rectangle(mouse.getX() - 1, mouse.getY() - 1, 2, 2);
		
		for (TitleButton button : buttons) {
			Rectangle rect = button.getBounds();
			if (mouseRect.intersects(rect)) any = true;
			g.setColor(Color.red);
			if (mouseRect.intersects(rect)) {
				g.fillRoundRect(-100, rect.y, rect.width + 150, rect.height, 150, 100);
				g.setColor(Color.white);
				g.setFont(new Font(oldFont.getFontName(), Font.PLAIN, 60));
				g.drawString(button.getName(), 70, rect.y + 70);
			} else {
				g.fillRoundRect(-100, rect.y, rect.width + 100, rect.height, 150, 100);
				g.setColor(Color.white);
				g.setFont(new Font(oldFont.getFontName(), Font.PLAIN, 60));
				g.drawString(button.getName(), 20, rect.y + 70);
			}
			
		}
		
		getDisplay().getRenderPane().setCursor(Cursor.getPredefinedCursor(any ? Cursor.HAND_CURSOR : Cursor.DEFAULT_CURSOR));
		
		g.setFont(oldFont);
		g.drawImage(image, 600, 400, 400, 600, null);
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getMouse();
		Rectangle mouseRect = new Rectangle(mouse.getX() - 1, mouse.getY() - 1, 2, 2);
		for (TitleButton button : buttons) {
			if (button.getBounds().intersects(mouseRect)) {
				Runnable runnable = button.getRunnable();
				if (runnable == null) continue;
				Sounds.click.play();
				runnable.run();
			}
		}
	}
	
	@Override
	public void update(double delta) {
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		getDisplay().setBackgroundColor(Color.black);
		Transition transition = new Transition();
		transition.fadeIn(30);
		transition.startTransition();
	}
	
}

package com.tobi.squared.screen;

import java.awt.Cursor;
import java.awt.Graphics;

import com.tobi.guibase.Mouse;
import com.tobi.squared.Squared;
import com.tobi.squared.SquaredDisplay;
import com.tobi.squared.Transition;

public abstract class Screen
{
	private Squared game;
	
	private static Screen selected = null;
	
	public Screen(Squared game) {
		this.game = game;
	}
	
	public abstract void render(Graphics g);
	public abstract void update(double delta);
	
	public void screenEntered(Screen lastScreen) {}
	public void screenExited(Screen nextScreen) {}
	public void keyPressed(int key, char ch) {}
	public void keyReleased(int key, char ch) {}
	public void mouseClicked(int mouseButton) {}
	public void mouseReleased(int mouseButton) {}
	public void mouseDragged() {}
	
	public static Screen getSelectedScreen() {
		return selected;
	}
	
	public static void setSelectedScreen(Screen screen) {
		Squared.getInstance().getDisplay().getRenderPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		Transition.getActiveTransitions().clear();
		Screen old = selected;
		Squared.getInstance().getDisplay().getMouse().releaseAllButtons();
		if (selected != null) {
			selected.screenExited(screen);
		}
		
		selected = screen;
		
		if (selected != null) {
			selected.screenEntered(old);
		}
	}
	
	public final Mouse getMouse() {
		return getDisplay().getMouse();
	}
	
	public final SquaredDisplay getDisplay() {
		return this.game.getDisplay();
	}
	
	public final Squared getGame() {
		return this.game;
	}
}

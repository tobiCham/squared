package com.tobi.squared.screen;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.tobi.squared.Difficulty;
import com.tobi.squared.GameResources;
import com.tobi.squared.Player;
import com.tobi.squared.Squared;
import com.tobi.squared.Transition;
import com.tobi.squared.data.LevelData;
import com.tobi.squared.screen.selectlevel.ScreenSelectLevel;
import com.tobi.squared.sound.Sound;
import com.tobi.squared.sound.Sounds;
import com.tobi.squared.tile.Tile;
import com.tobi.squared.tile.TileType;

public class ScreenLevel extends Screen {
	
	private List<Tile> tiles = new ArrayList<Tile>();
	private int startX, startY;
	private int levelNumber;
	private boolean levelpack;
	private ScreenLevelDesigner levelDesign;
	private float elapsedTime = 0;
	private int deaths = 0;
	private int tileSize;
	private Player player;
	private Map<TileType, List<Rectangle>> staticTiles = new HashMap<TileType, List<Rectangle>>();
	
	public ScreenLevel(Squared game, int tileSize, int levelNumber) {
		super(game);
		this.levelNumber = levelNumber;
		this.tileSize = tileSize;
		player = new Player(this);
		levelpack = false;
	}
	
	public ScreenLevel(Squared game, int tileSize, ScreenLevelDesigner returnScreen) {
		super(game);
		this.levelNumber = 2;
		this.tileSize = tileSize;
		player = new Player(this);
		levelpack = true;
		this.levelDesign = returnScreen;
	}
	
	public void findStartPos() {
		for (Tile tile : tiles) {
			if (tile.getTileType().equals(TileType.TILE_START)) {
				startX = tile.getX();
				startY = tile.getY();
				break;
			}
		}
		player.setX(startX);
		player.setY(startY);
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		getDisplay().setBackgroundColor(Color.black);
		getDisplay().resize(1024, 1024);
		
		Transition transition = new Transition();
		transition.fadeIn(30);
		transition.startTransition();
		if(!Sounds.levelMusic.isPlayable()) {
			Sounds.levelMusic.play();
		}
		
		elapsedTime = 0;
		if (levelNumber > 0) {
			getDisplay().getFrame().setTitle("Squared - Level " + levelNumber);
		}
		if (isLevelPack()) {
			getDisplay().getFrame().setTitle("Squared - Level Pack");
		}
	}
	
	@Override
	public void screenExited(Screen nextScreen) {
		if(nextScreen instanceof ScreenLevel) return;
		Sounds.levelMusic.pause();
		getDisplay().getFrame().setTitle("Squared");
	}
	
	@Override
	public void render(Graphics g) {
		if (GameResources.path == null) {
			g.setColor(Color.white);
			g.fillRect(0, 0, getGame().getWidth(), getGame().getHeight());
		} else {
			g.drawImage(GameResources.path, 0, 0, getGame().getWidth(), getGame().getHeight(), null);
		}
		for (Tile tile : getTiles()) {
			if(!tile.isStatic()) {
				tile.render(g);
			}
		}
		for(TileType type : staticTiles.keySet()) {
			g.setColor(type.getColor());
			for(Rectangle rect : staticTiles.get(type)) {
				g.fillRect(rect.x, rect.y, rect.width, rect.height);
			}
		}
		
		player.render(g, deaths);
		if (isLevelPack()) return;
		
		LevelData currentData = getGame().getDatabase().getData(levelNumber);
		if (currentData == null) currentData = new LevelData(levelNumber, Integer.MAX_VALUE, true);
		
		BigDecimal time = new BigDecimal(elapsedTime);
		if (time.floatValue() < 10) time = time.setScale(1, RoundingMode.HALF_UP);
		else time = time.setScale(0, RoundingMode.HALF_UP);
		
		g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, 40));
		String str = time.toString();
		int minutes = (int) (elapsedTime / 60);
		if (minutes > 0) {
			String strSeconds = ((int) (elapsedTime) % 60) + "";
			if (strSeconds.length() == 1) strSeconds = "0" + strSeconds;
			str = minutes + ":" + strSeconds;
		}
		Rectangle strBounds = g.getFontMetrics(g.getFont()).getStringBounds(str, g).getBounds();
		
		g.setColor(new Color(150, 150, 150, 150));
		g.fillRoundRect((getGame().getWidth() / 2) - (int) strBounds.getCenterX() - 10, 15, strBounds.width + 20, 45, 20, 20);
		
		if (currentData.getBestTime() == 0) g.setColor(Color.white);
		else if (elapsedTime < currentData.getBestTime()) g.setColor(Color.green);
		else g.setColor(Color.red);
		g.drawString(str + "", (getGame().getWidth() / 2) - (int) strBounds.getCenterX(), 50);
	}
	
	@Override
	public void update(double delta) {
		elapsedTime += delta / 1000000000f;
		
		getDisplay().getRenderPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		Sound sound = Sounds.levelMusic;
		
		if (!sound.isPlaying()) {
			sound.stop();
			sound.play();
		}
		
		for (Tile tile : getTiles()) {
			tile.update();
		}
		for (Tile tile : getTiles(player.getBoudingBox())) {
			if (tile.doesReset()) {
				playerDeath();
			}
		}
		player.update();
	}
	
	@Override
	public void keyPressed(int key, char ch) {
		if (key == KeyEvent.VK_ESCAPE) {
			if (levelpack) Screen.setSelectedScreen(levelDesign);
			else {
				ScreenSelectLevel selectLevel = new ScreenSelectLevel(getGame(), Difficulty.getByLevelNumber(levelNumber - 1));
				Screen.setSelectedScreen(selectLevel);
				Sounds.levelMusic.pause();
			}
			return;
		}
	}
	
	/**
	 * Resets the player to the start of the level, and restarts the timer
	 */
	public void playerDeath() {
		player.setX(getStartX());
		player.setY(getStartY());
		if (Transition.getActiveTransitions().isEmpty()) {
			Transition transition = new Transition();
			transition.fadeOut(30);
			transition.startTransition();
		}
		deaths++;
	}
	
	public void findBorders() {
		staticTiles.clear();
		
		int size = 1024 / tileSize;
		Set<Point> donePoints = new HashSet<Point>();
		Map<Point, TileType> tileMap = new HashMap<Point, TileType>();
		
		for(Tile tile : getTiles()) {
			if(tile.isStatic()) {
				int x = tile.getX() / tileSize;
				int y = tile.getY() / tileSize;
				tileMap.put(new Point(x, y), tile.getTileType());
			}
		}
		
		for(int j = 0; j < size; j++) {
			for(int i = 0; i < size; i++) {
				Point point = new Point(i, j);
				if(!tileMap.containsKey(point) || donePoints.contains(point)) continue;
				
				if(donePoints.contains(point)) continue;
				TileType type = tileMap.get(point);
					
				int right = 1, down = 0;
				
				for(int x = point.x + 1; x < size; x++) {
					Point p = new Point(x, point.y);
					if(tileMap.get(p) == type) right++;
					else break;
				}
				topLoop:
				for(int y = point.y; y < size; y++) {
					for(int x = point.x; x < point.x + right; x++) {
						Point p = new Point(x, y);
						if(tileMap.get(p) != type) {
							break topLoop;
						}
					}
					down++;
					for(int x = point.x; x < point.x + right; x++) donePoints.add(new Point(x, y));
				}
				
				Rectangle rect = new Rectangle(point.x * tileSize, point.y * tileSize, tileSize * right, tileSize * down);
				List<Rectangle> rectangles = staticTiles.get(type);
				if(rectangles == null) rectangles = new ArrayList<Rectangle>();
				rectangles.add(rect);
				staticTiles.put(type, rectangles);
			}
		}
	}
	
	public boolean canObjectGo(int x, int y) {
		if (x < 0 || y < 0 || x + tileSize > getGame().getWidth() || y + tileSize > getGame().getHeight()) return false;
		Rectangle posRect = new Rectangle(x, y, tileSize, tileSize);
		for (Tile tile : getTiles()) {
			Rectangle tileBox = new Rectangle(tile.getX(), tile.getY(), tileSize, tileSize);
			if (posRect.intersects(tileBox)) {
				if (tile.isSolid()) return false;
			}
		}
		return true;
	}
	
	public boolean canTileGo(int x, int y, Tile tile) {
		if (x < 0 || y < 0 || x + tileSize > getGame().getWidth() || y + tileSize > getGame().getHeight()) return false;
		for (Tile t2 : getTiles()) {
			if (t2 == tile) continue;
			if(!t2.isSolid()) continue;
			if (intersects(x, y, tileSize, tileSize, t2.getX(), t2.getY(), tileSize, tileSize)) {
				return false;
			}
		}
		return true;
	}
	
	private boolean intersects(int x1, int y1, int width1, int height1, int x2, int y2, int width2, int height2) {
		if (width1 <= 0 || width2 <= 0 || height1 <= 0 || height2 <= 0) return false;
		width1 += x1;
		height1 += y1;
		width2 += x2;
		height2 += y2;
		return ((width2 < x2 || width2 > x1) && (height2 < y2 || height2 > y1) && (width1 < x1 || width1 > x2) && (height1 < y1 || height1 > y2));
	}
	
	public Tile getTile(int x, int y) {
		Rectangle posRect = new Rectangle(x, y, tileSize, tileSize);
		for (Tile tile : getTiles()) {
			Rectangle tileBox = new Rectangle(tile.getX(), tile.getY(), tileSize, tileSize);
			if (posRect.intersects(tileBox)) { return tile; }
		}
		return null;
	}
	
	public List<Tile> getTiles(Rectangle rect) {
		List<Tile> tiles = new ArrayList<Tile>();
		for (Tile tile : getTiles()) {
			Rectangle tileBox = new Rectangle(tile.getX(), tile.getY(), tileSize, tileSize);
			if (rect.intersects(tileBox)) tiles.add(tile);
		}
		return tiles;
	}
	
	public List<Tile> getTiles(int x, int y, int width, int height) {
		List<Tile> tiles = new ArrayList<Tile>();
		for (Tile tile : getTiles()) {
			if(intersects(tile.getX(), tile.getY(), tileSize, tileSize, x, y, width, height)) tiles.add(tile);
		}
		return tiles;
	}
	
	public int getStartX() {
		return startX;
	}
	
	public void setStartX(int startX) {
		this.startX = startX;
	}
	
	public int getStartY() {
		return startY;
	}
	
	public void setStartY(int startY) {
		this.startY = startY;
	}
	
	public List<Tile> getTiles() {
		return tiles;
	}
	
	public int getLevelNumber() {
		return levelNumber;
	}
	
	public boolean isLevelPack() {
		return levelpack;
	}
	
	public ScreenLevelDesigner getDesignerScreen() {
		return levelDesign;
	}
	
	public float getTime() {
		return elapsedTime;
	}
	
	public int getTileSize() {
		return tileSize;
	}
}

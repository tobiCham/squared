package com.tobi.squared.screen;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.tobi.guibase.Mouse;
import com.tobi.squared.GameResources;
import com.tobi.squared.OptionButton;
import com.tobi.squared.ResourceLoader;
import com.tobi.squared.Squared;
import com.tobi.squared.tile.TileType;

public class ScreenLevelDesigner extends Screen {
	
	private ScreenLevelDesigner instance;
	private static Image grid = ResourceLoader.loadImage("grid.png");
	private static Image grid_32 = ResourceLoader.loadImage("grid-32.png");
	private TileType currentTile = TileType.TILE_PATH;
	private Map<Point, TileType> tiles;
	private JFileChooser chooser;
	private Map<TileType, List<Rectangle>> tileRenders;
	
	private int tileSelectSize = 64, tileSelectGap = 20, tileSelectIndent = 20, gridSize = 1040;
	private Point gridPos = new Point(280, 20);
	
	private int size = 16;
	
	private OptionButton<Object> saveImageButton, playLevelButton, importLevelButton, resetButton, backButton;
	private OptionButton<Integer> levelSizeButton;
	private OptionButton<?>[] buttons;
	
	public ScreenLevelDesigner(Squared game) {
		super(game);
		tileRenders = new HashMap<TileType, List<Rectangle>>();
		instance = this;
		
		File docs = new File(System.getProperty("user.home") + "/Documents/mylevel.level");
		chooser = new JFileChooser(docs);
		
		saveImageButton = new OptionButton<Object>(getGame(), 1370, 200, 500, 100, Color.black, Color.gray, Color.black, "Save Level Design", new Object[0]);
		saveImageButton.setOnClick(new Runnable() {
			
			@Override
			public void run() {
				saveLevel();
			}
		});
		
		playLevelButton = new OptionButton<Object>(getGame(), 1370, 800, 500, 100, Color.black, Color.gray, Color.black, "Test Level Design", new Object[0]);
		playLevelButton.setOnClick(new Runnable() {
			
			@Override
			public void run() {
				testLevel();
			}
		});
		
		importLevelButton = new OptionButton<Object>(getGame(), 1370, 350, 500, 100, Color.black, Color.gray, Color.black, "Import Level Design", new Object[0]);
		importLevelButton.setOnClick(new Runnable() {
			
			@Override
			public void run() {
				loadLevel();
			}
		});
		
		resetButton = new OptionButton<Object>(getGame(), 1370, 650, 500, 100, Color.black, Color.gray, Color.black, "Reset Level Design", new Object[0]);
		resetButton.setOnClick(new Runnable() {
			
			@Override
			public void run() {
				resetTiles();
			}
		});
		
		backButton = new OptionButton<Object>(getGame(), 1370, 950, 500, 100, Color.black, Color.gray, Color.black, "Back to Main Menu", new Object[0]);
		backButton.setOnClick(new Runnable() {
			
			@Override
			public void run() {
				Screen.setSelectedScreen(GameResources.title);
			}
		});
		
		levelSizeButton = new OptionButton<Integer>(getGame(), 1370, 500, 500, 100, Color.black, Color.gray, Color.black, "Level Size", new Integer[] { 16, 32 });
		levelSizeButton.setOnClick(new Runnable() {
			@Override
			public void run() {
				if (levelSizeButton.getValue() == size) return;
				size = levelSizeButton.getValue();
				resetTiles();
			}
		});
		levelSizeButton.setHoverText("Wipes current design!");
		buttons = new OptionButton[] { saveImageButton, playLevelButton, importLevelButton, resetButton, backButton, levelSizeButton };
		
		resetTiles();
	}
	
	
	@Override
	public void render(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, getGame().getWidth(), getGame().getHeight());
		g.setColor(Color.gray);
		g.fillRoundRect(tileSelectIndent, 25, 225, getGame().getHeight() - 50, 50, 50);
		g.setFont(new Font(g.getFont().getFamily(), Font.PLAIN, 40));
		g.setColor(Color.black);
		g.drawString("Tiles:", tileSelectIndent + 20, 80);
		g.setFont(new Font(g.getFont().getFamily(), Font.PLAIN, 20));
		
		int counter = 115;
		for (TileType type : TileType.values()) {
			if (type.getTexture() != null) g.drawImage(type.getTexture(), tileSelectIndent + 20, counter, tileSelectSize, tileSelectSize, null);
			else {
				g.setColor(type.getColor());
				g.fillRoundRect(tileSelectIndent + 20, counter, tileSelectSize, tileSelectSize, tileSelectSize / 2, tileSelectSize / 2);
			}
			
			g.setColor(Color.white);
			String[] strs = type.getName().split(" ");
			Rectangle2D rect = g.getFontMetrics(g.getFont()).getStringBounds(strs[0], g);
			int height = rect.getBounds().height * strs.length;
			int strY = (counter + tileSelectSize) - (height / 2) + (int) rect.getCenterY();
			int strCounter = 0;
			for (String name : strs) {
				g.drawString(name, tileSelectSize + tileSelectIndent + 40, strY + strCounter);
				strCounter += height / strs.length;
			}
			counter += tileSelectSize + tileSelectGap;
		}
		g.drawImage(GameResources.path, gridPos.x, gridPos.y, gridSize, gridSize, null);
		
//		float tileSize = (float) gridSize / size;
		for(TileType type : tileRenders.keySet()) {
			g.setColor(type.getColor());
			for(Rectangle rect : tileRenders.get(type)) {
				g.fillRect(rect.x, rect.y, rect.width, rect.height);
//				g.fillRect(rect.x, rect.y, (int) tileSize, (int) tileSize);
			}
		}
		
		if (size == 16) g.drawImage(grid, gridPos.x, gridPos.y, gridSize, gridSize, null);
		else g.drawImage(grid_32, gridPos.x, gridPos.y, gridSize, gridSize, null);
		
		g.setFont(new Font(g.getFont().getFamily(), Font.PLAIN, 30));
		g.setColor(Color.gray);
		g.fillRoundRect(1340, tileSelectIndent, 560, tileSelectSize * 2 + 10, tileSelectSize / 2, tileSelectSize / 2);
		g.setColor(Color.black);
		g.drawString("Selected Tile:", 1359, tileSelectIndent + 45);
		g.drawString(currentTile.getName(), 1350 + tileSelectSize + 20, tileSelectIndent + 105);
		
		if (currentTile.getTexture() != null) g.drawImage(currentTile.getTexture(), 1350, tileSelectIndent + 65, tileSelectSize, tileSelectSize, null);
		else {
			g.setColor(currentTile.getColor());
			g.fillRoundRect(1350, tileSelectIndent + 65, tileSelectSize, tileSelectSize, tileSelectSize / 2, tileSelectSize / 2);
		}
		
		g.setFont(new Font(g.getFont().getFamily(), Font.PLAIN, 40));
		
		boolean any = false;
		for (OptionButton<?> button : buttons) {
			button.render(g);
			if (button.isHovered()) any = true;
		}
		getDisplay().getRenderPane().setCursor(Cursor.getPredefinedCursor(any ? Cursor.HAND_CURSOR : Cursor.DEFAULT_CURSOR));
	}
	
	@Override
	public void update(double delta) {
		mouseDragged();
	}
	
	@Override
	public void mouseDragged() {
		Mouse mouse = getMouse();
		int x = mouse.getX();
		int y = mouse.getY();
		
		if (mouse.isButtonDown(Mouse.MOUSE_LEFT)) {
			float tileSize = (float) gridSize / size;
			if (x >= gridPos.x && x < gridPos.x + gridSize && y > gridPos.y && y < gridSize + gridPos.y) {
				float tileX = (x - gridPos.x) / tileSize;
				float tileY = (y - gridPos.y) / tileSize;
				placeTile((int) tileX, (int) tileY, currentTile);
			}
		}
	}
	
	private void updateTiles() {
		tileRenders = new HashMap<TileType, List<Rectangle>>();
		float tileSize = (float) gridSize / size;
		Set<Point> donePoints = new HashSet<Point>();

		for(int j = 0; j < size; j++) {
			for(int i = 0; i < size; i++) {
				Point point = new Point(i, j);
				if(!tiles.containsKey(point) || donePoints.contains(point)) continue;
				
				if(donePoints.contains(point)) continue;
				TileType type = tiles.get(point);
				if(type == null) type = TileType.TILE_PATH;
				if (type == TileType.TILE_PATH) continue;

				int right = 1, down = 0;
				
				for(int x = point.x + 1; x < size; x++) {
					Point p = new Point(x, point.y);
					if(tiles.get(p) == type) right++;
					else break;
				}
				topLoop:
				for(int y = point.y; y < size; y++) {
					for(int x = point.x; x < point.x + right; x++) {
						Point p = new Point(x, y);
						if(tiles.get(p) != type) {
							break topLoop;
						}
					}
					down++;
					for(int x = point.x; x < point.x + right; x++) donePoints.add(new Point(x, y));
				}
				
				List<Rectangle> rects = tileRenders.get(type);
				if(rects == null) rects = new ArrayList<Rectangle>();
				rects.add(new Rectangle((int) (gridPos.x + (point.x * tileSize)), gridPos.y + (int) ((point.y * tileSize)), (int) (tileSize * right), (int) (tileSize * down)));
				tileRenders.put(type, rects);
			}
		}
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		getDisplay().setBackgroundColor(Color.gray);
		getDisplay().resize(1920, 1080);
		getDisplay().getFrame().setTitle("Squard - Level Designer");
		updateTiles();
	}
	
	@Override
	public void screenExited(Screen nextScreen) {
		getDisplay().setBackgroundColor(Color.black);
		getDisplay().resize(1024, 1024);
		getDisplay().getFrame().setTitle("Squard");
	}
	
	@Override
	public void keyPressed(int key, char ch) {
		if (key == KeyEvent.VK_ESCAPE) Screen.setSelectedScreen(GameResources.title);
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getMouse();
		int x = mouse.getX();
		int y = mouse.getY();
		for (OptionButton<?> button : buttons) {
			button.mouseClick(mouseButton, x, y);
		}
		
		float tileSize = (float) gridSize / size;
		if (x >= gridPos.x && x < gridPos.x + gridSize && y > gridPos.y && y < gridSize + gridPos.y) {
			float tileX = (x - gridPos.x) / tileSize;
			float tileY = (y - gridPos.y) / tileSize;
			placeTile((int) tileX, (int) tileY, currentTile);
		}
		
		if (x < tileSelectIndent + 20 || x > tileSelectIndent + 20 + tileSelectSize) return;
		int counter = 0;
		for (TileType type : TileType.values()) {
			int tileY = 115 + (tileSelectGap + tileSelectSize) * counter;
			if (y >= tileY && y <= tileY + tileSelectSize) currentTile = type;
			counter++;
		}
	}
	
	public void testLevel() {
		if (!tiles.containsValue(TileType.TILE_START)) {
			JOptionPane.showMessageDialog(getDisplay().getFrame(), "Please include a Starting tile!", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		if (!tiles.containsValue(TileType.TILE_END)) {
			JOptionPane.showMessageDialog(getDisplay().getFrame(), "Please include a Ending Tile!", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		BufferedImage image = convertToImage();
		getDisplay().resize(1024, 1024);
		ScreenLevel screen = getGame().getLoader().loadLevelpack(image, instance);
		if (screen != null) Screen.setSelectedScreen(screen);
		else getDisplay().resize(1920, 1080);
	}
	
	private void loadLevel() {
		chooser.setDialogTitle("Open Level");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setFileFilter(new FileNameExtensionFilter("Level Files", "level"));
		int result = chooser.showOpenDialog(getDisplay().getFrame());
		if (result != JFileChooser.APPROVE_OPTION) return;
		File file = chooser.getSelectedFile();
		if (file.isDirectory() || !file.exists()) {
			JOptionPane.showMessageDialog(getDisplay().getFrame(), "Please select a valid file", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		BufferedImage image = null;
		try {
			image = (BufferedImage) ImageIO.read(file);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(getDisplay().getFrame(), "Please select a valid file", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		loadFromImage(image);
	}
	
	private void loadFromImage(BufferedImage image) {
		Integer[] sizes = levelSizeButton.getValues();
		boolean any = false;
		for (int size : sizes) {
			if (image.getWidth() == size && image.getHeight() == size) {
				any = true;
				break;
			}
		}
		if (!any) {
			JOptionPane.showMessageDialog(getDisplay().getFrame(), "Please select a valid file", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		size = image.getWidth();
		levelSizeButton.setValue(size);
		resetTiles();
		for (int i = 0; i < image.getWidth(null); i++) {
			for (int j = 0; j < image.getHeight(null); j++) {
				int color = image.getRGB(i, j);
				TileType type = TileType.getTileFromColor(new Color(color));
				tiles.put(new Point(i, j), type);
			}
		}
		updateTiles();
	}
	
	private void saveLevel() {
		chooser.setDialogTitle("Save Level");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setFileFilter(new FileNameExtensionFilter("Level Files", ".level"));
		int result = chooser.showSaveDialog(getDisplay().getFrame());
		if (result != JFileChooser.APPROVE_OPTION) return;
		File file = chooser.getSelectedFile();
		if (file.isDirectory()) {
			JOptionPane.showMessageDialog(getDisplay().getFrame(), "Please select a valid file", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		if (!hasExtension(file)) file = new File(file.getAbsolutePath() + ".level");
		if (file.exists()) {
			int overwriteOption = JOptionPane.showConfirmDialog(getDisplay().getFrame(), "File exists already. Overwrite?", "File Exists", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
			if (overwriteOption == JOptionPane.NO_OPTION) return;
		}
		writeAsImage(file);
	}
	
	public void placeTile(int x, int y, TileType type) {
		if (tiles.containsValue(TileType.TILE_START) && type == TileType.TILE_START) return;
		if (tiles.containsValue(TileType.TILE_END) && type == TileType.TILE_END) return;
		tiles.put(new Point(x, y), type);
		
		updateTiles();
	}
	
	public BufferedImage convertToImage() {
		BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
		Graphics g = image.getGraphics();
		for (Point point : tiles.keySet()) {
			if(tiles.get(point) == null) g.setColor(TileType.TILE_PATH.getColor());
			else g.setColor(tiles.get(point).getColor());
			g.fillRect(point.x, point.y, 1, 1);
		}
		return image;
	}
	
	private static boolean hasExtension(File file) {
		String name = file.getName();
		int lastIndex = name.lastIndexOf(".");
		if (lastIndex == -1) return false;
		if (lastIndex + 2 >= name.length()) return false;
		return true;
	}
	
	public boolean writeAsImage(File output) {
		try {
			ImageIO.write(convertToImage(), "png", output);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public void resetTiles() {
		currentTile = TileType.TILE_PATH;
		tiles = new HashMap<Point, TileType>();
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				tiles.put(new Point(i, j), TileType.TILE_BLOCK);
			}
		}
		updateTiles();
	}
}

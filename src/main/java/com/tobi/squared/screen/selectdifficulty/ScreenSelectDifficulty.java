package com.tobi.squared.screen.selectdifficulty;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import com.tobi.guibase.Mouse;
import com.tobi.squared.Difficulty;
import com.tobi.squared.GameResources;
import com.tobi.squared.OptionButton;
import com.tobi.squared.Squared;
import com.tobi.squared.screen.Screen;
import com.tobi.squared.sound.Sounds;

public class ScreenSelectDifficulty extends Screen {
	
	private List<OptionButton<?>> buttons = new ArrayList<OptionButton<?>>();
	private final int NUMB_BUTTONS = 4;
	
	public ScreenSelectDifficulty(Squared game) {
		super(game);
		
		int width = 800;
		int height = 200;
		int x = (getGame().getWidth() / 2) - (width / 2);
		int gapY = 50;
		int totalHeight = (NUMB_BUTTONS * (height + gapY)) - gapY;
		int startY = (getGame().getHeight() / 2) - (totalHeight / 2);
		
		int counter = 0;
		for (Difficulty difficulty : Difficulty.values()) {
			ButtonSelectDifficulty button = new ButtonSelectDifficulty(getGame(), x, (counter * (height + gapY)) + startY, width, height, difficulty);
			buttons.add(button);
			counter++;
		}
		
		OptionButton<Object> button = new OptionButton<Object>(getGame(), x, (counter * (height + gapY)) + startY, width, height, Color.red, Color.red, Color.white, "Back", new Object[0]);
		button.setOnClick(new Runnable() {
			
			@Override
			public void run() {
				Screen.setSelectedScreen(GameResources.title);
			}
		});
		buttons.add(button);
	}
	
	@Override
	public void render(Graphics g) {
		Font oldFont = g.getFont();
		g.setFont(new Font(oldFont.getFontName(), Font.PLAIN, 100));
		
		boolean any = false;
		for (OptionButton<?> button : buttons) {
			button.render(g);
			if (button.isHovered()) any = true;
		}
		getDisplay().getRenderPane().setCursor(Cursor.getPredefinedCursor(any ? Cursor.HAND_CURSOR : Cursor.DEFAULT_CURSOR));
		g.setFont(oldFont);
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		getDisplay().setBackgroundColor(Color.BLACK);
		Sounds.levelMusic.setPosition(0);
	}
	
	@Override
	public void keyPressed(int key, char ch) {
		if (key == KeyEvent.VK_ESCAPE) Screen.setSelectedScreen(GameResources.title);
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		Mouse mouse = getMouse();
		for (OptionButton<?> button : buttons) {
			button.mouseClick(mouseButton, mouse.getX(), mouse.getY());
		}
	}
	
	@Override
	public void update(double delta) {}
}

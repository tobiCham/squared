package com.tobi.squared.screen.selectdifficulty;

import java.awt.Color;

import com.tobi.squared.Difficulty;
import com.tobi.squared.OptionButton;
import com.tobi.squared.Squared;
import com.tobi.squared.screen.Screen;
import com.tobi.squared.screen.selectlevel.ScreenSelectLevel;

public class ButtonSelectDifficulty extends OptionButton<Object> {
	
	private Difficulty difficulty;
	
	public ButtonSelectDifficulty(Squared game, int x, int y, int width, int height, Difficulty difficulty) {
		super(game, x, y, width, height, Color.red, Color.red, Color.white, getName(difficulty), new Object[0]);
		this.difficulty = difficulty;
		setOnClick(new Runnable() {
			
			@Override
			public void run() {
				ScreenSelectLevel screen = new ScreenSelectLevel(Squared.getInstance(), getDifficulty());
				Screen.setSelectedScreen(screen);
			}
		});
	}
	
	public Difficulty getDifficulty() {
		return difficulty;
	}
	
	private static String getName(Difficulty difficulty) {
		StringBuilder builder = new StringBuilder();
		String[] words = difficulty.name().split(" ");
		for (String word : words) {
			word = word.toLowerCase();
			if (word.length() == 0) continue;
			if (word.length() == 1) builder.append(word.toUpperCase() + " ");
			else builder.append((word.charAt(0) + "").toUpperCase() + word.substring(1, word.length()) + " ");
		}
		return builder.toString().trim();
	}
	
}

package com.tobi.squared.screen;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;

import com.tobi.squared.GameResources;
import com.tobi.squared.OptionButton;
import com.tobi.squared.Squared;
import com.tobi.squared.sound.Sounds;

public class ScreenOptions extends Screen {
	
	private OptionButton<?>[] buttons;
	
	private OptionButton<Boolean> musicButton, soundsButton;
	private OptionButton<Object> saveChanges;
	private OptionButton<Object> discardChanges;
	
	public ScreenOptions(Squared game) {
		super(game);
		
		musicButton = new OptionButton<Boolean>(getGame(), 467 + 50, 50, 462, 100, Color.black, Color.red, Color.white, "Music Enabled", new Boolean[] { true, false });
		soundsButton = new OptionButton<Boolean>(getGame(), 25, 50, 462, 100, Color.black, Color.red, Color.white, "Sounds Enabled", new Boolean[] { true, false });
		
		saveChanges = new OptionButton<Object>(getGame(), 20, getGame().getHeight() - 100 - 50, 467, 100, Color.black, Color.red, Color.white, "Save Changes", new Object[0]);
		saveChanges.setOnClick(new Runnable() {
			
			@Override
			public void run() {
				getGame().getConfig().setMusic(musicButton.getValue());
				getGame().getConfig().setSound(soundsButton.getValue());
				Sounds.updateSettings();
				try {
					getGame().getConfig().save();
				} catch (Exception e) {
					System.err.println("Error saving to properties file.");
					e.printStackTrace();
				}
				Screen.setSelectedScreen(GameResources.title);
			}
		});
		
		discardChanges = new OptionButton<Object>(getGame(), 467 + 50, getGame().getHeight() - 100 - 50, 467, 100, Color.black, Color.red, Color.white, "Discard Changes", new Object[0]);
		discardChanges.setOnClick(new Runnable() {
			
			@Override
			public void run() {
				Screen.setSelectedScreen(GameResources.title);
				getMouse().releaseAllButtons();
			}
		});
		buttons = new OptionButton<?>[] {musicButton, soundsButton, saveChanges, discardChanges};
	}
	
	@Override
	public void screenEntered(Screen lastScreen) {
		musicButton.setValue(getGame().getConfig().hasMusic());
		soundsButton.setValue(getGame().getConfig().hasSound());
	}
	
	public void render(Graphics g) {
		g.setFont(new Font(g.getFont().getFamily(), Font.PLAIN, 42));
		boolean any = false;
		for (OptionButton<?> button : buttons) {
			button.render(g);
			if (button.isHovered()) any = true;
		}
		getDisplay().getRenderPane().setCursor(Cursor.getPredefinedCursor(any ? Cursor.HAND_CURSOR : Cursor.DEFAULT_CURSOR));
	}
	
	@Override
	public void mouseClicked(int mouseButton) {
		for (OptionButton<?> button : buttons) {
			button.mouseClick(mouseButton, getMouse().getX(), getMouse().getY());
		}
	}
	
	@Override
	public void keyPressed(int key, char ch) {
		if (key == KeyEvent.VK_ESCAPE) Screen.setSelectedScreen(GameResources.title);
	}
	
	@Override
	public void update(double delta) {}
}

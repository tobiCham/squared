package com.tobi.squared.sound;

import java.net.URL;

import com.tobi.squared.Squared;
import com.tobi.squared.data.SquaredConfig;

public class Sounds {

	public static SoundClip levelMusic;
	public static SoundClip click;
	
	public static void init() {
		levelMusic = loadClip("Hyperfun.mid");
		Squared.getInstance().setLoadingProgress(0.35f);
		
		click = loadClip("click.wav");
		levelMusic.setVolume(0.98f);
		
		updateSettings();
	}
	
	public static void updateSettings() {
		SquaredConfig config = Squared.getInstance().getConfig();
		levelMusic.setPlayable(config.hasMusic());
		click.setPlayable(config.hasSound());
	}
	
	private static SoundClip loadClip(String name) {
		URL url = Squared.class.getResource("/" + name);
		SoundClip clip = new SoundClip(url);
		Squared instance = Squared.getInstance();
		instance.setLoadingProgress(instance.getLoadingProgress() + 0.2f);
		return clip;
	}
}

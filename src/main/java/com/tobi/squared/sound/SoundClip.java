package com.tobi.squared.sound;

import java.net.URL;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public class SoundClip implements Sound {
	
	private Clip clip;
	private int point = 0;
	private boolean loaded = false;
	private boolean playable = true;
	
	public SoundClip(URL url) {
		try {
			if (url == null) load(null);
			load(url.toString());
		} catch (Exception e) {
			System.err.println("Failed to load sound effect " + url);
			loaded = false;
		}
	}
	
	@Override
	public void load(String audioName) {
		try {
			URL url = new URL(audioName);
			clip = AudioSystem.getClip();
			clip.open(AudioSystem.getAudioInputStream(url));
			loaded = true;
		} catch (Exception e) {
			System.err.println("Failed to load sound effect " + audioName);
			loaded = false;
		}
	}
	
	@Override
	public void play() {
		if (!loaded || !playable) return;
		this.clip.setFramePosition(point);
		this.clip.start();
	}
	
	@Override
	public void stop() {
		if (!loaded || !playable) return;
		this.clip.setFramePosition(1);
		point = 1;
		this.clip.stop();
	}
	
	@Override
	public void pause() {
		if (!loaded || !playable) return;
		point = this.clip.getFramePosition();
		this.clip.stop();
	}
	
	@Override
	public void setVolume(float vol) {
		if (!loaded || !playable) return;
		FloatControl gainControl = (FloatControl) this.clip.getControl(FloatControl.Type.MASTER_GAIN);
		float difference = Math.abs(0 - gainControl.getMinimum());
		float newVol = (vol * difference) + gainControl.getMinimum();
		gainControl.setValue(newVol);
	}
	
	@Override
	public float getVolume() {
		if (!loaded || !playable) return 0f;
		FloatControl gainControl = (FloatControl) this.clip.getControl(FloatControl.Type.MASTER_GAIN);
		float percentage = gainControl.getValue() / gainControl.getMaximum();
		return percentage;
	}
	
	@Override
	public void setPosition(int framePosition) {
		if (!loaded || !playable) return;
		clip.setFramePosition(framePosition);
	}
	
	@Override
	public int getPosition() {
		if (!loaded) return 0;
		return clip.getFramePosition();
	}
	
	@Override
	public boolean isPlaying() {
		if (!loaded || !playable) return true;
		return this.clip.isRunning();
	}
	
	public Clip getClip() {
		return clip;
	}
	
	@Override
	public void setPlayable(boolean enabled) {
		this.playable = enabled;
	}
	
	@Override
	public boolean isPlayable() {
		return playable;
	}
	
	public boolean hasLoaded() {
		return loaded;
	}
}

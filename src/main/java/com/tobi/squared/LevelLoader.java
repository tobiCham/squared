package com.tobi.squared;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import com.tobi.squared.screen.ScreenLevel;
import com.tobi.squared.screen.ScreenLevelDesigner;
import com.tobi.squared.tile.Tile;
import com.tobi.squared.tile.TileType;

public class LevelLoader {

	private Squared game;
	
	public LevelLoader(Squared game) {
		this.game = game;
	}
	
	public ScreenLevel loadLevelpack(BufferedImage image, ScreenLevelDesigner designer) {
		int tileSize = game.getWidth() / image.getWidth();
		
		ScreenLevel level = new ScreenLevel(game, tileSize, designer);
		game.getDisplay().getFrame().setTitle("Squared - Level Pack");
		loadTiles(image, level);
		return level;
	}

	public ScreenLevel loadLevel(int id) {
		URL url = Squared.class.getResource("/levels/level" + id + ".level");
		if (url == null) return null;
		
		BufferedImage image = readImage("levels/level" + id + ".level");
		int tileSize = game.getWidth() / image.getWidth();
		
		ScreenLevel level = new ScreenLevel(game, tileSize, id);
		game.getDisplay().getFrame().setTitle("Squared - Level " + id);

		loadTiles(image, level);
		return level;
	}
	
	private void loadTiles(BufferedImage image, ScreenLevel level) {
		int tileSize = game.getWidth() / image.getWidth();
		
		BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics g = newImage.getGraphics();
		g.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
		g.dispose();
		
		for(int i = 0; i < newImage.getWidth(); i++) {
			for(int j = 0; j < newImage.getHeight(); j++) {
				Color color = new Color(newImage.getRGB(i, j));
				TileType type = TileType.getTileFromColor(color);
				if(type == null) type = TileType.TILE_START;
				Tile tile = type.instantiate(level, i * tileSize, j * tileSize);
				if(tile != null) level.getTiles().add(tile);
			}
		}
		level.findStartPos();
		level.findBorders();
	}
	
	private BufferedImage readImage(String name) {
		try {
			URL url = Squared.class.getResource("/" + name);
			return ImageIO.read(url);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}

package com.tobi.squared;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import com.tobi.guibase.GUIBase;
import com.tobi.squared.screen.Screen;

public class SquaredDisplay extends GUIBase {

	private Squared game;
	
	private Color backgroundColor;
	private Image loadingImage;
	
	public SquaredDisplay(Squared game) {
		super(1024, 1024, true, true, "Squared");
		this.game = game;
		
		setRenderSetting(new SquaredRenderSetting());
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		getFrame().setSize((int) (screenSize.height / 1.2), (int) (screenSize.height / 1.2));
		
		setMaintainAspectRatio(true);
		getFrame().setIconImage(ResourceLoader.loadImage("icon.png"));
		
		loadingImage = ResourceLoader.loadImage("splash.png");
		backgroundColor = Color.BLACK;
	}

	@Override
	public void render(Graphics g) {
		if (game.getLoadingProgress() < 1) {
			g.setColor(Color.black);
			g.fillRect(0, 0, getWidth(), getHeight());
			g.drawImage(loadingImage, 0, 0, getWidth(), getHeight(), null);
			int loadingBarIndent = 100;
			g.setColor(Color.red);
			g.fillRect(loadingBarIndent, 170, getWidth() - (loadingBarIndent * 2), 50);
			g.setColor(Color.green);
			g.fillRect(loadingBarIndent, 170, (int) ((getWidth() - (loadingBarIndent * 2)) * game.getLoadingProgress()), 50);
			return;
		}
	
		g.setFont(GameResources.font);
		if (Screen.getSelectedScreen() != null) {
			Screen.getSelectedScreen().render(g);
		}
		Transition.updateAll(g, game);
	}
	
	@Override
	public void renderBackground(Graphics g, int paneWidth, int paneHeight) {
		g.setColor(backgroundColor);
		g.fillRect(0, 0, paneWidth, paneHeight);
	}

	@Override
	public void update(int updateTime) {
		if (game.getLoadingProgress() < 1) return;
		if (Screen.getSelectedScreen() != null) Screen.getSelectedScreen().update(updateTime);
	}
	
	@Override
	public void mouseClick(int mouseButton) {
		if (Screen.getSelectedScreen() != null) Screen.getSelectedScreen().mouseClicked(mouseButton);
	}
	
	@Override
	public void mouseRelease(int mouseButton) {
		if (Screen.getSelectedScreen() != null) Screen.getSelectedScreen().mouseReleased(mouseButton);
	}
	
	@Override
	public void keyPress(int keycode, char ch) {
		if (Screen.getSelectedScreen() != null) Screen.getSelectedScreen().keyPressed(keycode, ch);
	}
	
	@Override
	public void keyRelease(int keycode, char ch) {
		if (Screen.getSelectedScreen() != null) Screen.getSelectedScreen().keyReleased(keycode, ch);
	}
	
	@Override
	public void mouseDrag() {
		if (Screen.getSelectedScreen() != null) Screen.getSelectedScreen().mouseDragged();
	}

	@Override
	public void windowClosed() {
		game.saveData();
	}

	public Color getBackgroundColor() {
		return backgroundColor;
	}
	
	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
}

package com.tobi.squared.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.tobi.squared.Difficulty;

public class DatabaseManager {
	
	private File file;
	private Map<Integer, LevelData> levelMap = new HashMap<Integer, LevelData>();
	
	public DatabaseManager(File databaseFile) throws IOException {
		this.file = databaseFile;
		load();
	}
	
	public LevelData getData(int level) {
		LevelData data = levelMap.get(level);
		if(data != null) return data;
		
		return addNewData(level);
	}
	
	public void addMissingLevels() {
		boolean missing = false;
		for(int i = 1; i <= Difficulty.HARD.getEndLevel(); i++) {
			if(!levelMap.containsKey(i)) {
				addNewData(i);
				missing = true;
			}
		}
		if(missing) save();
	}
	
	private LevelData addNewData(int level) {
		LevelData data = new LevelData(level, 0, level == 1);
		levelMap.put(level, data);
		return data;
	}
	
	public synchronized void save() {
		JsonObject object = new JsonObject();
		for(Entry<Integer, LevelData> entry : levelMap.entrySet()) {
			JsonObject dataObject = new JsonObject();
			dataObject.addProperty("time", entry.getValue().getBestTime());
			dataObject.addProperty("unlocked", entry.getValue().isUnlocked());
			object.add(Integer.toString(entry.getKey()), dataObject);
		}
		try(PrintWriter writer = new PrintWriter(file)) {
			writer.println(new GsonBuilder().setPrettyPrinting().create().toJson(object));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private void load() throws IOException {
		if(!file.exists()) return;
		
		StringBuilder builder = new StringBuilder();
		try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
			while(true) {
				String line = reader.readLine();
				if(line == null) break;
				builder.append(line).append("\n");
			}
		}
		JsonObject object = new Gson().fromJson(builder.toString(), JsonObject.class);
		for(Entry<String, JsonElement> entry : object.entrySet()) {
			int levelID = Integer.parseInt(entry.getKey());
			
			JsonObject dataObject = entry.getValue().getAsJsonObject();
			float bestTime = dataObject.get("time").getAsFloat();
			boolean unlocked = dataObject.get("unlocked").getAsBoolean();
			
			levelMap.put(levelID, new LevelData(levelID, bestTime, unlocked));
		}
	}
}

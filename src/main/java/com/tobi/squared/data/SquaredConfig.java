package com.tobi.squared.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class SquaredConfig {

	private Properties properties;
	private File file;
	
	private static Map<String, Object> defaultValues = new HashMap<>();
	
	static {
		defaultValues.put("sound", true);
		defaultValues.put("music", true);
	}
	
	public SquaredConfig(File file) {
		this.properties = new Properties();
		this.file = file;
	}

	public void load() throws IOException {
		properties.clear();
		
		if(!file.exists()) file.createNewFile();
		try(FileInputStream fileIn = new FileInputStream(file)) {
			properties.load(fileIn);
		}
		boolean missing = false;
		for(String val : defaultValues.keySet()) {
			if(properties.getProperty(val) == null) {
				System.out.println(val);
				missing = true;
				properties.setProperty(val, defaultValues.get(val) + "");
			}
		}
		if(missing) save();
	}
	
	public void save() throws IOException {
		try(FileOutputStream output = new FileOutputStream(file)) {
			properties.store(output, null);
			output.flush();
		}
	}
	
	public boolean hasSound() {
		return Boolean.parseBoolean(properties.getProperty("sound", "true"));
	}
	
	public void setSound(boolean enabled) {
		properties.setProperty("sound", enabled + "");
	}
	
	public boolean hasMusic() {
		return Boolean.parseBoolean(properties.getProperty("music", "true"));
	}
	
	public void setMusic(boolean enabled) {
		properties.setProperty("music", enabled + "");
	}
}

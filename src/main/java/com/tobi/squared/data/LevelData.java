package com.tobi.squared.data;

public class LevelData {
	
	private final int level;
	private float bestTime;
	private boolean unlocked;
	
	public LevelData(int level, float bestTime, boolean completed) {
		this.bestTime= bestTime;
		this.level = level;
		this.unlocked = completed;
	}
	
	public LevelData(int level, float bestTime) {
		this(level, bestTime, false);
	}

	public float getBestTime() {
		return bestTime;
	}

	public void setBestTime(float bestTime) {
		this.bestTime = bestTime;
	}

	public int getLevel() {
		return level;
	}

	public boolean isUnlocked() {
		if(level == 1) return true;
		return unlocked;
	}

	public void setUnlocked(boolean unlocked) {
		this.unlocked = unlocked;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("level=");
		builder.append(level);
		builder.append(", bestTime=");
		builder.append(bestTime);
		builder.append(", unlocked=");
		builder.append(unlocked);
		return builder.toString();
	}
}

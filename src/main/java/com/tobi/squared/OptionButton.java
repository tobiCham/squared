package com.tobi.squared;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

import com.tobi.squared.sound.Sounds;

public class OptionButton<T> {
	
	private T[] values;
	private int selectedIndex;
	private String text;
	private String hoverText;
	private int x, y, width, height;
	private Color edgeColor, insideColor, textColor;
	private Runnable onClick;
	private Squared game;
	private float roundness = 0.2f;
	
	public OptionButton(Squared game, int x, int y, int width, int height, Color edgeColor, Color insideColor, Color textColor, String text, T[] values) {
		this.values = values;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.text = text;
		this.edgeColor = edgeColor;
		this.insideColor = insideColor;
		this.textColor = textColor;
		this.game = game;
	}
	
	public void render(Graphics g) {
		if (values == null) return;
		if (selectedIndex >= values.length) {
			selectedIndex = 0;
		}
		
		int x = this.x;
		int y = this.y;
		int width = this.width;
		int height = this.height;
		boolean hover = false;
		
		if (isHovered()) {
			int expansion = (int) (width * 0.05);
			x -= expansion;
			width += expansion * 2;
			hover = true;
		}
		
		Color orig = g.getColor();
		g.setColor(edgeColor);
		g.fillRoundRect(x - 2, y - 2, width + 4, height + 4, (int) (roundness * width), (int) (roundness * width));
		g.setColor(insideColor);
		g.fillRoundRect(x, y, width, height, (int) (roundness * width), (int) (roundness * width));
		T value = null;
		if (selectedIndex < values.length) value = values[selectedIndex];
		String str = text;
		g.setColor(textColor);
		if (value != null) str += ": " + value.toString();
		Rectangle boundingBox = g.getFontMetrics(g.getFont()).getStringBounds(str, g).getBounds();
		g.drawString(str, (int) (x + (width / 2) - boundingBox.getCenterX()), (int) (y + (height / 2) - boundingBox.getCenterY()));
		if (hover && hoverText != null) {
			int size = g.getFont().getSize();
			g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, (int) (size / 1.5)));
			Rectangle newStringBox = g.getFontMetrics(g.getFont()).getStringBounds(hoverText, g).getBounds();
			g.drawString(hoverText, (int) (x + (width / 2) - newStringBox.getCenterX()), (int) (y + newStringBox.getCenterY()));
			g.setFont(new Font(g.getFont().getFontName(), Font.PLAIN, size));
		}
		g.setColor(orig);
	}
	
	public boolean isHovered() {
		Rectangle rect = new Rectangle(x - 2, y - 2, width + 4, height + 4);
		Rectangle mouse = new Rectangle(game.getDisplay().getMouse().getX() - 1, game.getDisplay().getMouse().getY() - 1, 3, 3);
		return rect.intersects(mouse);
	}
	
	public void mouseClick(int button, int x, int y) {
		Rectangle buttonRect = new Rectangle(this.x, this.y, this.width, this.height);
		Rectangle mouseRect = new Rectangle(x - 1, y - 1, 3, 3);
		if (buttonRect.intersects(mouseRect)) {
			game.getDisplay().getMouse().releaseAllButtons();
			selectedIndex++;
			if (selectedIndex >= values.length) selectedIndex = 0;
			Sounds.click.play();
			if (onClick != null) onClick.run();
		}
	}
	
	public T getValue() {
		return values[selectedIndex];
	}
	
	public int getSelectedIndex() {
		return selectedIndex;
	}
	
	public void setSelectedIndex(int selectedIndex) {
		this.selectedIndex = selectedIndex;
	}
	
	/**
	 * @param value
	 *            The value to set to
	 * @return Whether the operation was successful.
	 */
	public boolean setValue(T value) {
		for (int i = 0; i < values.length; i++) {
			T t = values[i];
			if (t.equals(value)) {
				setSelectedIndex(i);
				return true;
			}
		}
		return false;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getWidth() {
		return width;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public Color getEdgeColor() {
		return edgeColor;
	}
	
	public void setEdgeColor(Color edgeColor) {
		this.edgeColor = edgeColor;
	}
	
	public Color getInsideColor() {
		return insideColor;
	}
	
	public void setInsideColor(Color insideColor) {
		this.insideColor = insideColor;
	}
	
	public Color getTextColor() {
		return textColor;
	}
	
	public void setTextColor(Color textColor) {
		this.textColor = textColor;
	}
	
	public Runnable getOnClick() {
		return onClick;
	}
	
	public void setOnClick(Runnable onClick) {
		this.onClick = onClick;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getHoverText() {
		return hoverText;
	}
	
	public void setHoverText(String hoverText) {
		this.hoverText = hoverText;
	}
	
	public T[] getValues() {
		return values;
	}
	
	public void setRoundnessRatio(float ratio) {
		if (ratio < 0) ratio = 0;
		if (ratio > 1) ratio = 1;
		this.roundness = ratio;
	}
	
	public void setBounds(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
}

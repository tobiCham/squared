package com.tobi.squared;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Transition {
	
	private static List<Transition> transitions = new ArrayList<Transition>();
	
	public void startTransition() {
		transitions.add(this);
	}
	
	private int fadeInTimer;
	private int fadeOutTimer;
	private int startIn, startOut;
	private Color oldColor;
	private boolean running;
	private boolean finished = false;
	
	public boolean hasFinished() {
		return !running;
	}
	
	public void fadeIn(int time) {
		fadeInTimer = time;
		startIn = time;
		running = true;
	}
	
	public void fadeOut(int time) {
		fadeOutTimer = time;
		startOut = time;
		running = true;
	}
	
	public void renderTransition(Graphics g, Squared game) {
		if (fadeInTimer > 0) {
			oldColor = g.getColor();
			float percentage = (float) fadeInTimer / startIn;
			g.setColor(new Color(0, 0, 0, (int) (percentage * 255)));
			g.fillRect(0, 0, game.getWidth(), game.getHeight());
			fadeInTimer -= 1;
			g.setColor(oldColor);
		}
		if (fadeOutTimer > 0) {
			oldColor = g.getColor();
			float percentage = (float) fadeOutTimer / startOut;
			g.setColor(new Color(0, 0, 0, (int) (percentage * 255)));
			g.fillRect(0, 0, game.getWidth(), game.getHeight());
			fadeOutTimer -= 1;
			g.setColor(oldColor);
		}
		
		if (fadeInTimer < 1 && fadeOutTimer < 1) {
			finished = true;
			running = false;
		}
	}
	
	public static void updateAll(Graphics g, Squared game) {
		List<Transition> transitions = new ArrayList<Transition>();
		for (Transition trans : Transition.transitions)
			transitions.add(trans);
		Iterator<Transition> iterator = transitions.iterator();
		while (iterator.hasNext()) {
			Transition transition = iterator.next();
			transition.renderTransition(g, game);
			if (transition.finished) {
				iterator.remove();
			}
		}
		Transition.transitions = transitions;
	}
	
	public static List<Transition> getActiveTransitions() {
		return transitions;
	}
}

package com.tobi.squared;

import java.awt.RenderingHints;

import com.tobi.guibase.RenderSetting;

public class SquaredRenderSetting extends RenderSetting {

	public SquaredRenderSetting() {
		super("Squared");
		
		addOption(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		addOption(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		addOption(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
		addOption(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
		
		addOption(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);
		addOption(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
		addOption(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
	}
}

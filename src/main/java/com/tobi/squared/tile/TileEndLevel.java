package com.tobi.squared.tile;

import com.tobi.squared.Difficulty;
import com.tobi.squared.GameResources;
import com.tobi.squared.Squared;
import com.tobi.squared.data.LevelData;
import com.tobi.squared.screen.Screen;
import com.tobi.squared.screen.ScreenLevel;

public class TileEndLevel extends Tile {
	
	public TileEndLevel(ScreenLevel level, int x, int y) {
		super(TileType.TILE_END, level, x, y);
	}
	
	@Override
	public void playerOnTile() {
		ScreenLevel level = (ScreenLevel) Screen.getSelectedScreen();
		if (level.isLevelPack()) {
			Screen.setSelectedScreen(level.getDesignerScreen());
		} else {
			boolean updateNext = false;
			boolean updateCurrent = false;
			
			Difficulty difficulty = Difficulty.getByLevelNumber(level.getLevelNumber() - 1);
			int nextLevel = level.getLevelNumber() + 1;
			final LevelData nextData = Squared.getInstance().getDatabase().getData(nextLevel);
			if (!nextData.isUnlocked()) {
				nextData.setUnlocked(true);
				updateNext = true;
			}
			
			final LevelData currentData = Squared.getInstance().getDatabase().getData(nextLevel - 1);
			if (level.getTime() < currentData.getBestTime() || currentData.getBestTime() == 0) {
				currentData.setBestTime(level.getTime());
				updateCurrent = true;
			}
			if (!currentData.isUnlocked()) {
				currentData.setUnlocked(true);
				updateCurrent = true;
			}
			
			final boolean updateNextFlag = updateNext;
			final boolean updateCurrentFlag = updateCurrent;
			
			if (updateNextFlag || updateCurrentFlag) {
				new Thread() {
					@Override
					public void run() {
						Squared.getInstance().getDatabase().save();
					}
				}.start();
			}
			
			Squared.getInstance().saveData();
			if (difficulty.getEndLevel() == nextLevel - 1) {
				Difficulty newDifficulty = Difficulty.getByLevelNumber(nextLevel);
				if (newDifficulty == null) {
					Screen.setSelectedScreen(GameResources.title);
					return;
				}
			}
			ScreenLevel screen = Squared.getInstance().getLoader().loadLevel(nextLevel);
			if (screen == null) {
				Screen.setSelectedScreen(GameResources.title);
				return;
			}
			Screen.setSelectedScreen(screen);
		}
	}
	
	@Override
	public boolean isStatic() {
		return false;
	}
}

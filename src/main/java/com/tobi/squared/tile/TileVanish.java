package com.tobi.squared.tile;

import java.awt.Color;
import java.awt.Graphics;

import com.tobi.squared.Squared;
import com.tobi.squared.screen.ScreenLevel;

public class TileVanish extends Tile {
	
	private int counter = 0;
	
	public TileVanish(ScreenLevel level, int x, int y) {
		super(TileType.TILE_VANISH, level, x, y);
		counter = Squared.RANDOM.nextInt(60);
	}
	
	@Override
	public void update() {
		counter--;
		if (counter <= 0) {
			counter = 45;
			setVisible(!isVisible());
		}
	}
	
	@Override
	public void render(Graphics g) {
		if (isVisible()) super.render(g);
		else {
			Color orig = getColor();
			g.setColor(new Color(orig.getRed(), orig.getGreen(), orig.getBlue(), 100));
			int size = level.getTileSize();
			g.fillRect(getX(), getY(), size, size);
		}
	}
	
	@Override
	public boolean doesReset() {
		return isVisible();
	}
	
	@Override
	public boolean isStatic() {
		return false;
	}
}

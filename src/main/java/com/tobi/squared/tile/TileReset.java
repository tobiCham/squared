package com.tobi.squared.tile;

import com.tobi.squared.screen.ScreenLevel;

public class TileReset extends Tile {
	
	
	public TileReset(ScreenLevel level, int x, int y) {
		super(TileType.TILE_RESET, level, x, y);
	}
	
	@Override
	public boolean doesReset() {
		return true;
	}
	
	@Override
	public boolean isStatic() {
		return true;
	}
}

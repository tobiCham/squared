package com.tobi.squared.tile;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import com.tobi.squared.screen.ScreenLevel;

public abstract class Tile {
	
	protected int x, y;
	protected ScreenLevel level;
	private Color color;
	private boolean visible = true, solid = false, resets = false;
	private TileType tileType;
	private Image texture;
	
	
	public Tile(TileType tileType, ScreenLevel level, int x, int y) {
		this.x = x;
		this.y = y;
		this.tileType = tileType;
		this.color = tileType.getColor();
		this.texture = tileType.getTexture();
		this.level = level;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	public void render(Graphics g) {
		if (isVisible()) {
			if (texture != null) {
				int tileSize = level.getTileSize();
				g.drawImage(texture, getX(), getY(), tileSize, tileSize, null);
			} else {
				Color color = getColor();
				g.setColor(color);
				g.fillRect(getX(), getY(), level.getTileSize(), level.getTileSize());
			}
		}
	}
	
	public Image getTexture() {
		return texture;
	}
	
	public void update() {
		
	}
	
	public void playerOnTile() {
		
	}
	
	public boolean isVisible() {
		return visible;
	}
	
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	public boolean isSolid() {
		return solid;
	}
	
	public void setSolid(boolean solid) {
		this.solid = solid;
	}
	
	public boolean doesReset() {
		return resets;
	}
	
	public void setResets(boolean resets) {
		this.resets = resets;
	}
	
	public TileType getTileType() {
		return tileType;
	}
	
	public boolean isStatic() {
		return false;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (obj instanceof TileType) {
			TileType tiles = (TileType) obj;
			if (tiles.getTileClass().equals(this.getClass())) return true;
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		return (x + "").hashCode() + (y + "").hashCode() + (color.getRGB() + "").hashCode();
	}
	
	@Override
	public String toString() {
		return "[" + getTileType().toString() + ": " + x + ", " + y + "]";
	}
}

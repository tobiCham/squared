package com.tobi.squared.tile;

import com.tobi.squared.screen.Screen;
import com.tobi.squared.screen.ScreenLevel;

public class TileVerticalMove extends Tile {
	
	private int velocity;
	
	public TileVerticalMove(ScreenLevel level, int x, int y) {
		super(TileType.TILE_VERTICAL_MOVE, level, x, y);
		velocity = (int) (3 * (level.getTileSize() / 32f));
	}
	
	@Override
	public void update() {
		int newPos = getY() + velocity;
		ScreenLevel level = (ScreenLevel) Screen.getSelectedScreen();
		if (level.canTileGo(getX(), newPos, this)) y = newPos;
		else velocity *= -1;
	}
	
	@Override
	public boolean isSolid() {
		return true;
	}
	
	@Override
	public boolean doesReset() {
		return true;
	}
	
	@Override
	public boolean isStatic() {
		return false;
	}
}

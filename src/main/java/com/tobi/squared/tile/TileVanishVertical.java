package com.tobi.squared.tile;

import java.awt.Color;
import java.awt.Graphics;

import com.tobi.squared.screen.Screen;
import com.tobi.squared.screen.ScreenLevel;

public class TileVanishVertical extends Tile {
	
	private int velocity;
	private int counter = 0;
	
	public TileVanishVertical(ScreenLevel level, int x, int y) {
		super(TileType.TILE_VANISH_VERTICAL_MOVE, level, x, y);
		velocity = (int) (3 * (level.getTileSize() / 32f));
	}
	
	@Override
	public void update() {
		counter--;
		if (counter <= 0) {
			counter = 45;
			setVisible(!isVisible());
		}
		
		int newPos = getY() + velocity;
		ScreenLevel level = (ScreenLevel) Screen.getSelectedScreen();
		if (level.canTileGo(getX(), newPos, this)) y = newPos;
		else velocity *= -1;
	}
	
	@Override
	public void render(Graphics g) {
		if (isVisible()) super.render(g);
		else {
			Color orig = getColor();
			g.setColor(new Color(orig.getRed(), orig.getGreen(), orig.getBlue(), 100));
			int size = level.getTileSize();
			g.fillRect(getX(), getY(), size, size);
		}
	}
	
	@Override
	public boolean isSolid() {
		return isVisible();
	}
	
	@Override
	public boolean doesReset() {
		return isVisible();
	}
	
	@Override
	public boolean isStatic() {
		return false;
	}
}

package com.tobi.squared.tile;

import com.tobi.squared.screen.Screen;
import com.tobi.squared.screen.ScreenLevel;

public class TileHorizontalMove extends Tile {
	
	private int velocity;
	
	public TileHorizontalMove(ScreenLevel level, int x, int y) {
		super(TileType.TILE_HORIZONTAL_MOVE, level, x, y);
		velocity = (int) (3 * (level.getTileSize() / 32f));
	}
	
	@Override
	public void update() {
//		int newPos = getX() + velocity;
//		int size = Squared.getInstance().getTileSize();
//		if (newPos < 0 || newPos + size > Squared.getInstance().getInitialWidth()) {
//			velocity *= -1;
//			return;
//		}
//		List<Tile> tiles = ((ScreenLevel) Screen.getSelectedScreen()).getTiles(newPos, getY(), size, size);
//		for (Tile tile : tiles) {
//			if (tile == this) continue;
//			if (tile.isSolid()) {
//				velocity *= -1;
//				return;
//			}
//		}
//		setX(newPos);
		
		int newPos = x + velocity;
		ScreenLevel level = (ScreenLevel) Screen.getSelectedScreen();
		if(level.canTileGo(newPos, getY(), this)) x = newPos;
		else velocity *= -1;
	}
	
	@Override
	public boolean isSolid() {
		return true;
	}
	
	@Override
	public boolean doesReset() {
		return true;
	}
	
	@Override
	public boolean isStatic() {
		return false;
	}
}

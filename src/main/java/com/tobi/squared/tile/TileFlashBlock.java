package com.tobi.squared.tile;

import com.tobi.squared.Squared;
import com.tobi.squared.screen.ScreenLevel;

public class TileFlashBlock extends Tile {
	
	private int counter = 0;
	
	public TileFlashBlock(ScreenLevel level, int x, int y) {
		super(TileType.TILE_FLASH_BLOCK, level, x, y);
		counter = Squared.RANDOM.nextInt(60);
	}
	
	@Override
	public void update() {
		counter--;
		if (counter <= 0) {
			counter = 45;
			setVisible(!isVisible());
		}
	}
	
	@Override
	public boolean isSolid() {
		return isVisible();
	}
	
	@Override
	public boolean isStatic() {
		return false;
	}
	
}

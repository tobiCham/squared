package com.tobi.squared.tile;

import com.tobi.squared.screen.ScreenLevel;

public class TileStartLevel extends Tile {
	
	public TileStartLevel(ScreenLevel level, int x, int y) {
		super(TileType.TILE_START, level, x, y);
	}
	
	@Override
	public boolean isVisible() {
		return false;
	}
	
	@Override
	public boolean isStatic() {
		return false;
	}
}

package com.tobi.squared.tile;
import java.awt.Color;
import java.awt.Image;
import java.lang.reflect.Constructor;
import java.net.URL;

import com.tobi.squared.ResourceLoader;
import com.tobi.squared.Squared;
import com.tobi.squared.screen.ScreenLevel;

public enum TileType {
	
	TILE_START(TileStartLevel.class, Color.BLUE, "Start", "start"), 
	TILE_END(TileEndLevel.class, Color.RED, "End", "end"), 
	TILE_BLOCK(TileBlock.class, Color.BLACK, "Block", "block"),
	TILE_PATH(TilePath.class, Color.WHITE, "Path", "path"),
	TILE_VANISH(TileVanish.class, Color.GREEN, "Vanish", "vanish"),
	TILE_RESET(TileReset.class, Color.YELLOW, "Reset", "reset"),
	TILE_HORIZONTAL_MOVE(TileHorizontalMove.class, Color.MAGENTA, "Horizontal", "horizontal_move"),
	TILE_VERTICAL_MOVE(TileVerticalMove.class, Color.CYAN, "Vertical", "vertical_move"),
	TILE_FLASH_BLOCK(TileFlashBlock.class, new Color(100, 100, 100), "Vanishing Block", "flash_block"),
	TILE_VANISH_HORIZONTAL_MOVE(TileVanishHorizontal.class, new Color(110, 0, 110), "Vanishing Horizontal", "vanish_horizontal_move"), 
	TILE_VANISH_VERTICAL_MOVE(TileVanishVertical.class, new Color(255, 110, 0), "Vanishing Vertical", "vanish_vertical_move"); 

	private Class<? extends Tile> tileClass;
	private Color color;
	//Used for level packs.
	private String name;
	private Image texture;

	private TileType(Class<? extends Tile> tileClass, Color color, String name, String textureName) {
		this.tileClass = tileClass;
		this.color = color;
		this.name = name;
		URL url = Squared.class.getResource("/tiles/" + textureName + ".png");
		if (url != null) texture = ResourceLoader.loadImage("tiles/" + textureName + ".png");
	}
	
	public Class<? extends Tile> getTileClass() {
		return tileClass;
	}
	
	public Color getColor() {
		return color;
	}
	
	public String getName() {
		return name;
	}
	
	public Image getTexture() {
		return texture;
	}
	
	public static TileType getTileFromColor(Color color) {
		for (TileType tile : values()) {
			if (tile.getColor().equals(color)) return tile;
		}
		return null;
	}
	
	public Tile instantiate(ScreenLevel level, int x, int y) {
		try {
			Constructor<?> constructor = getTileClass().getDeclaredConstructor(ScreenLevel.class, int.class, int.class);
			return (Tile) constructor.newInstance(level, x, y);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}

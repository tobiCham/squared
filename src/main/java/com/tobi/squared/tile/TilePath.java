package com.tobi.squared.tile;

import com.tobi.squared.screen.ScreenLevel;

public class TilePath extends Tile {
	
	public TilePath(ScreenLevel level, int x, int y) {
		super(TileType.TILE_PATH, level, x, y);
	}
	
	@Override
	public boolean isVisible() {
		return false;
	}
	
	@Override
	public boolean isStatic() {
		return false;
	}
}
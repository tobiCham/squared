package com.tobi.squared.tile;

import com.tobi.squared.screen.ScreenLevel;

public class TileBlock extends Tile {
	
	public TileBlock(ScreenLevel level, int x, int y) {
		super(TileType.TILE_BLOCK, level, x, y);
	}
	
	@Override
	public boolean isSolid() {
		return true;
	}

	@Override
	public boolean isStatic() {
		return true;
	}
}

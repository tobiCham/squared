package com.tobi.squared;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.tobi.squared.data.DatabaseManager;
import com.tobi.squared.data.LevelData;
import com.tobi.squared.data.SquaredConfig;

public class Squared {
	
	public static final Random RANDOM = new Random();
	
	private static Squared instance;
	
	private File saveFolder;
	
	private SquaredDisplay display;
	private SquaredConfig config;
	private DatabaseManager manager;
	public LevelLoader loader;
	
	private float loading = 0;
	
	public static void main(String[] args) {
		new Squared();
	}
	
	public Squared() {
		instance = this;
		try {
			loadSettings();
		} catch (IOException e) {
			e.printStackTrace();
			crash(e);
			return;
		}
		
		display = new SquaredDisplay(this);
		display.open();
		
		actualInit();
	}
	
	public static void crash(Exception e) {
		StringBuilder stacktrace = new StringBuilder();
		stacktrace.append("//Send this to the developer\n-------Stacktrace-------\n\n");
		stacktrace.append(e.getClass() == null ? "" : (e.getClass().getName() + ": ") + e.getMessage());
		for(StackTraceElement element : e.getStackTrace()) {
			stacktrace.append("\n   at " + element.toString());
		}
		
		JFrame parent = null;
		if(instance != null && instance.display != null) parent = instance.display.getFrame();
		
		JOptionPane.showMessageDialog(parent, "The game just crashed :(\n\n" + stacktrace.toString().trim(), "Error", JOptionPane.ERROR_MESSAGE);
		System.exit(-1);
	}
	
	private void loadSettings() throws IOException {
		String os = System.getProperty("os.name");
		if (os.toLowerCase().contains("win")) {
			saveFolder = new File(System.getenv("AppData"));
		} else {
			File userHome = new File(System.getProperty("user.home"));
			saveFolder = new File(userHome, "Library/Application Support");
		}
		
		saveFolder = new File(saveFolder, "Squared");
		if (!saveFolder.exists()) saveFolder.mkdirs();
		
		config = new SquaredConfig(new File(saveFolder, "/settings.prop"));
		config.load();
	}
	
	private void actualInit() {
		try {
			manager = new DatabaseManager(new File(saveFolder, "leveldata.json"));
			manager.addMissingLevels();
			
			LevelData data = manager.getData(1);
			if (!data.isUnlocked()) {
				data.setUnlocked(true);
				manager.save();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		loader = new LevelLoader(this);
		loading = 0.1f;
		GameResources.init(this);
		loading = 1f;
	}
	
	public static Squared getInstance() {
		return instance;
	}
	
	public SquaredConfig getConfig() {
		return config;
	}
	
	public void saveData() {
		try {
			config.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public float getLoadingProgress() {
		return loading;
	}
	
	public void setLoadingProgress(float progress) {
		if (progress < 0) progress = 0;
		if (progress > 1) progress = 1;
		this.loading = progress;
	}
	
	public int getWidth() {
		return display.getWidth();
	}
	
	public int getHeight() {
		return display.getHeight();
	}
	
	public SquaredDisplay getDisplay() {
		return display;
	}
	
	public DatabaseManager getDatabase() {
		return manager;
	}
	
	public LevelLoader getLoader() {
		return loader;
	}
}

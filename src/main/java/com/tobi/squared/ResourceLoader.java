package com.tobi.squared;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class ResourceLoader {
	private static Image defaultImage;
	
	public static Font loadFont(String name) {
		try {
			URL fontUrl = Squared.class.getResource("/" + name);
			Font font = Font.createFont(Font.TRUETYPE_FONT, fontUrl.openStream());
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(font);
			Font newFont = new Font(font.getName(), font.getStyle(), 12);
			return newFont;
		} catch (Exception e) {
			System.err.println("Error loading font\"" + name + "\": " + e.getMessage());
		}
		return null;
	}
	
	public static Image getDefaultImage() {
		if (defaultImage != null) return defaultImage;
		
		BufferedImage image = new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB);
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				if (((i % 2) == 0 && (j % 2) == 0) || (i % 2) != 0 && (j % 2) != 0) image.setRGB(i, j, Color.magenta.getRGB());
				else image.setRGB(i, j, Color.BLACK.getRGB());
			}
		}
		return image;
	}
	
	public static Image loadImage(String name) {
		URL url = Squared.class.getResource("/" + name);
		if (url == null) return defaultImage;
		try {
			return ImageIO.read(url);
		} catch (IOException e) {
			System.err.println("Error loading image \"" + name + "\": " + e.getMessage());
			return getDefaultImage();
		}
	}
	
	public static Image loadImage(String name, int newWidth, int newHeight) {
		Image image = loadImage(name);
		if (image == null) image = getDefaultImage();
		if (image.getWidth(null) == newWidth && image.getHeight(null) == newHeight) return image;
		BufferedImage newImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics g = newImage.getGraphics();
		g.drawImage(image, 0, 0, newWidth, newHeight, null);
		g.dispose();
		return newImage;
	}
}

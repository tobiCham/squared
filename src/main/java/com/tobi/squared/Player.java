package com.tobi.squared;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.List;

import com.tobi.guibase.Keyboard;
import com.tobi.squared.screen.Screen;
import com.tobi.squared.screen.ScreenLevel;
import com.tobi.squared.tile.Tile;

public class Player {
	
	private double x = 0, y = 0;
	private double volX = 1, volY = 1;
	private ScreenLevel level;
	
	public Player(ScreenLevel level) {
		this.level = level;
		updateVelocity();
	}
	
	public double getX() {
		return x;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public double getY() {
		return y;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public double getVolX() {
		return volX;
	}
	
	public void setVolX(double volX) {
		this.volX = volX;
	}
	
	public double getVolY() {
		return volY;
	}
	
	public void setVolY(double volY) {
		this.volY = volY;
	}
	
	public void update() {
		if (!(Screen.getSelectedScreen() instanceof ScreenLevel)) return;
		ScreenLevel level = (ScreenLevel) Screen.getSelectedScreen();
		double newX = x;
		double newY = y;
		if (Keyboard.isKeyDown(KeyEvent.VK_LEFT)) newX -= volX;
		if (Keyboard.isKeyDown(KeyEvent.VK_RIGHT)) newX += volX;
		if (Keyboard.isKeyDown(KeyEvent.VK_UP)) newY -= volY;
		if (Keyboard.isKeyDown(KeyEvent.VK_DOWN)) newY += volY;
		
		if (level.canObjectGo((int) newX, (int) y)) x = newX;
		if (level.canObjectGo((int) x, (int) newY)) y = newY;
		List<Tile> tiles = level.getTiles(new Rectangle((int) x, (int) y, level.getTileSize(), level.getTileSize()));
		for (Tile t : tiles) t.playerOnTile();
		updateVelocity();
	}
	
	public void render(Graphics g, int deaths) {
		g.setColor(Color.blue);
		g.fillRect((int) x, (int) y, level.getTileSize(), level.getTileSize());
		g.setColor(Color.white);
	}
	
	public Rectangle getBoudingBox() {
		return new Rectangle((int) x, (int) y, level.getTileSize(), level.getTileSize());
	}
	
	public void updateVelocity() {
		volX = level.getTileSize() / 8f;
		volY = level.getTileSize() / 8f;
	}
}

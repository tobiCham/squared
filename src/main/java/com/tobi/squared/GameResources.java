package com.tobi.squared;

import java.awt.Font;
import java.awt.Image;

import javax.swing.JEditorPane;

import com.tobi.squared.screen.Screen;
import com.tobi.squared.screen.ScreenLevelDesigner;
import com.tobi.squared.screen.ScreenOptions;
import com.tobi.squared.screen.selectdifficulty.ScreenSelectDifficulty;
import com.tobi.squared.screen.title.ScreenTitle;
import com.tobi.squared.sound.Sounds;
import com.tobi.squared.tile.TileType;

public class GameResources {
	
	public static Font font = new JEditorPane().getFont();
	public static Screen title, selectDifficulty, screenOptions, levelDesigner;
	public static Image padlock, path;
	
	public static void init(Squared instance) {
		TileType[] tiles = TileType.values();
		for (TileType type : tiles) {
			type.getColor();
		}
		Font font = ResourceLoader.loadFont("Font.ttf");
		instance.setLoadingProgress(0.2f);
		Sounds.init();
		instance.setLoadingProgress(0.5f);
		if (font != null) GameResources.font = font;
		path = ResourceLoader.loadImage("path.png");
		title = new ScreenTitle(instance);
		instance.setLoadingProgress(0.6f);
		selectDifficulty = new ScreenSelectDifficulty(instance);
		instance.setLoadingProgress(0.7f);
		screenOptions = new ScreenOptions(instance);
		instance.setLoadingProgress(0.8f);
		levelDesigner = new ScreenLevelDesigner(instance);
		instance.setLoadingProgress(0.9f);
		padlock = ResourceLoader.loadImage("padlock.png");
		Screen.setSelectedScreen(title);
	}
}

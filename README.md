# Squared

## Overview
A tile based game where you have to traverse the level to reach the exit, avoiding obstacles which can send you back to the start of the level.
Has multiple levels, with each one being progressively harder than the previous.

The game also allows you to create custom levels, and share them with friends!

## Requirements
Requires Java 7 or higher to be able to run. Download via the "Downloads" tab

## Demo
The [following video](https://youtu.be/aHVvLSTd0Pg) demonstrates some of the game's features